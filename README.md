# Postgresql 14 Single

Run Docker Command
```
docker-compose up --build -d
```

Change Time Zone
```
docker exec -it pgsql-14 ln -sf /usr/share/zoneinfo/Asia/Jakarta /etc/localtime
```
```
docker exec -it pgsql-14 date
```
